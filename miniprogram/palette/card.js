export default class LastMayday {
  palette() {
    return ({
      width: '670rpx',
      height: '1040rpx',
      borderRadius: '50rpx',
      views: [{
          type: 'image',
          url: 'https://classmate.zhicloudcn.com/api//202004291845180241826.png',
          css: {
            borderRadius: '16rpx',
            width: '670rpx',
            height: '1040rpx',
          },
        },

        {   
          // 头像
          type: 'image',
          url: '/palette/avatar.jpg',
          css: {
            top: '55rpx',
            right: '70rpx',
            borderRadius: '100rpx',
            width: '110rpx',
            height: '110rpx',
            borderColor: '#fff',
            borderWidth: '4rpx',
          },
        },

        {
          // name
          type: 'text',
          text: 'name',
          css: [{
            top: '180rpx',
            color: '#fff',
            fontSize: '30rpx',
            right: '70rpx',
            textAlign: 'right',
          }],
        },
        {
          type: 'text',
          text: '的同学录',
          css: [{
            top: '222rpx',
            right: '134rpx',
            align: 'center',
            width: '126rpx',
            textAlign: 'right',
            fontSize: '30rpx',
            color: '#fff',
          }]
        },

        {
          // 二维码
          type: 'image',
          url: '/palette/avatar.jpg',
          css: {
            bottom: '27rpx',
            right: '50rpx',
            width: '140rpx',
            height: '140rpx',
          },
        },
      ],
    });
  }
}


function _textDecoration(decoration, index, color) {
  return ({
    type: 'text',
    text: decoration,
    css: [{
      top: `${startTop + index * gapSize}rpx`,
      color: color,
      textDecoration: decoration,
    }, common],
  });
}

function _image(index, rotate, borderRadius) {
  return ({
    type: 'image',
    url: '/palette/avatar.jpg',
    css: {
      top: `${startTop + 8.5 * gapSize}rpx`,
      left: `${startLeft + 160 * index}rpx`,
      width: '120rpx',
      height: '120rpx',
      shadow: '10rpx 10rpx 5rpx #888888',
      rotate: rotate,
      borderRadius: borderRadius,
    },
  });
}

function _des(index, content) {
  const des = {
    type: 'text',
    text: content,
    css: {
      fontSize: '22rpx',
      top: `${startTop + 8.5 * gapSize + 140}rpx`,
    },
  };
  if (index === 3) {
    des.css.right = '60rpx';
  } else {
    des.css.left = `${startLeft + 120 * index + 30}rpx`;
  }
  return des;
}