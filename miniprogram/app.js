// 83886  83882
// 83895
let interstitialAd = null
App({
  data: {
    userInfo: '',
    code: '',
    letter_img: 'https://classmate.zhicloudcn.com/api//202005271206490229563.jpg', 
    headUrl: 'https://classmate.lodidc.cn/api/',
  },
  onLaunch: function (e) {

  },
  onShow: function (options) {},

  wx_getWhereuserinfo() {
    return new Promise((resolve, reject) => {
      var that = this
      wx.login({
        success: res => {
          that.data.code = res.code
          wx.request({
            method: 'post',
            url: that.data.headUrl + 'Member/getWhereuserinfo',
            data: {
              code: that.data.code,
            },
            success(res) {
              if (res.statusCode == '200') {
                if (res.data.status == '200') { //存在数据，老用户
                  resolve(res)
                  wx.setStorageSync('token', res.data.token)
                  wx.setStorageSync('user_info', res.data.data)
                } else { //新用户，进行登录授权操作
                  resolve('new')
                }
              }
            }
          })
        },
        fail: function (err) {
          console.log('获取用户登录态失败！' + err)
        },
      })
    })
  },
  wx_login() {
    var that = this
    return new Promise((resolve, reject) => {
      wx.login({
        success: res => {
          that.data.code = res.code
          resolve(res)
        },
        fail: function (err) {
          console.log('获取用户登录态失败！' + err)
        },
      })
    })
  },
  wx_getUserInfo() {
    return new Promise((resolve, reject) => {
      var that = this;
      wx.getUserInfo({
        success: function (res) {
          wx.request({
            method: 'post',
            url: that.data.headUrl + 'Member/xcxlogin',
            data: {
              'code': that.data.code,
              'encryptedData': res.encryptedData,
              'iv': res.iv
            },
            success(res) {
              if (res.statusCode == '200') {
                wx.setStorageSync('token', res.data.token)
                that.data.userInfo = res.data.data
                resolve(res.data.data)
                wx.setStorageSync('user_info', res.data.data)
              } else {
                reject(res)
                console.log("请求失败，在前端打印的信息", res)
              }
            }
          })
        }
      })
    })
  },
  // 实时获取用户信息
  get_info: function () {
    let that = this;
    return new Promise((resolve, reject) => {
      wx.request({
        header: {
          Authorization: wx.getStorageSync('token')
        },
        url: that.data.headUrl + 'Member/User',
        method: 'POST',
        success(res) {
          if (res.statusCode == '200') {
            resolve(res)
          } else {
            reject(res)
            console.log("请求失败，在前端打印的信息", res)
          }
        }
      })
    })
  },

  //上传
  wx_uploadFile(name, path) {
    let that = this;
    return new Promise((resolve, reject) => {
      wx.uploadFile({
        header: {
          Authorization: wx.getStorageSync('token')
        },
        url: that.data.headUrl + 'Base/upload',
        filePath: path,
        name: name,
        success(res) {
          if (res.statusCode == '200') {
            resolve(res)
          } else {
            reject(res)
            console.log("请求失败，在前端打印的信息", res)
          }
        }
      })
    })
  },

  //封裝get请求
  get_request(url) {
    let that = this;
    return new Promise((resolve, reject) => {
      wx.request({
        method: 'get',
        url: that.data.headUrl + url,
        success(res) {
          if (res.statusCode == '200') {
            resolve(res)
          } else {
            reject(res)
            console.log("请求失败，在前端打印的信息", res)
          }
        }
      })
    })
  },
  //封裝post请求
  post_request(url, re_data) {
    let that = this;
    return new Promise((resolve, reject) => {
      wx.request({
        method: 'post',
        url: that.data.headUrl + url,
        data: re_data,
        success(res) {
          if (res.statusCode == '200') {
            resolve(res)
          } else {
            reject(res)
            console.log("请求失败，在前端打印的信息", res)
          }
        }
      })
    })
  },
  //获取学校
  school_request(url) {
    let that = this;
    return new Promise((resolve, reject) => {
      wx.request({
        header: {
          Authorization: wx.getStorageSync('token')
        },
        method: 'get',
        url: that.data.headUrl + url,
        success(res) {
          if (res.statusCode == '200') {
            resolve(res.data.data)
          } else {
            reject(res)
            console.log("请求失败，在前端打印的信息", res)
          }
        }
      })
    })
  },

  wx_uplogin() {
    var that = this
    var info = wx.getStorageSync('user_info')
    wx.getSetting({
      success(res) {
        if (!res.authSetting['scope.userInfo']) {

        } else {
          wx.login({
            success: res => {
              var code = res.code
              wx.getUserInfo({
                success: function (res) {
                  var encryptedData = res.encryptedData
                  var iv = res.iv
                  wx.request({
                    method: 'post',
                    url: that.data.headUrl + 'Member/uplogin',
                    data: {
                      id: info.id,
                      code: code,
                      encryptedData: encryptedData,
                      iv: iv
                    }
                  })
                }
              })
            },
            fail: function (err) {
              console.log('获取uplogin失败！' + err)
            },
          })
        }
      }
    })
  },


  //广告创建，如开通流量主则把注释去掉
  createAdv: function () {
    // if (wx.createInterstitialAd) {
    // 	//1.创建广告实例
    // 	interstitialAd = wx.createInterstitialAd({
    // 		adUnitId: 'adunit-fff1adcd58106a6a'
    // 	})
    // 	interstitialAd.onLoad(() => {
    // 		console.log('插屏 广告加载成功')
    // 	})
    // 	interstitialAd.onError((err) => {
    // 		console.log('广告拉取失败', err)
    // 	})
    // }
  },
  showAdv() {
    // if (interstitialAd) {
    // 	interstitialAd.show().catch((res) => {
    // 		console.error(res, "广告组件显示")
    // 	})
    // }
  },
  wx_showSuccess(text) {
    wx.showToast({
      title: text,
      icon: 'success',
      mask: true,
      duration: 2000,

    })
  },
  wx_loginTip() {
    wx.showLoading({
      title: '登录中...',
      duration: 6000,
      mask: true,
    })
  },

  wx_showFail(text) {
    wx.showToast({
      title: text,
      icon: none,
      duration: 2000
    })
  },
  wx_modalTip(text, content) {
    wx.showModal({
      title: text,
      content: content,
      showCancel: false,
    })
  },
  wx_loginOk() {
    wx.hideLoading()
    wx.showToast({
      title: '授权成功，数据更新',
      duration: 2000,
      icon: 'none',
      mask: true,
    })
  },
})