var ctx = wx.createCanvasContext('shareCanvas')
import Card from '../../palette/card';
const app = getApp()
Page({
	imagePath: '',
	data: {
		list: [
			'https://classmate.zhicloudcn.com/api//202004291845180241826.png',
			'https://classmate.zhicloudcn.com/api//202004291844250227902.png',
			'https://classmate.zhicloudcn.com/api//202004291845350295497.png',
			'https://classmate.zhicloudcn.com/api//202004291846010273722.png',
			'https://classmate.zhicloudcn.com/api//202004291846060219886.png',
			'https://classmate.zhicloudcn.com/api//202004291846100208405.png',
		],
		now_index: 0,
		qrcode: '',
		template: {},
		views: ''
	},
	onImgOK(e) {
		this.imagePath = e.detail.path;
	},
	onLoad() {
		var that = this
		wx.showLoading({
			title: '海报加载中...',
			mask: true,
		})

		var now_index = this.data.now_index
		const userInfo = wx.getStorageSync('user_info')
		this.setData({
			path: this.data.list[now_index],
			id: userInfo.id,
			username: userInfo.username,
			headimgurl: userInfo.headimgurl
		})
		
		var url = 'Base/qrcode'
		var data = {
			scene: that.data.id,
			page: 'pages/check-friend/index',
			filename: that.data.id,
		}
		app.post_request(url, data).then(function (res) {
			that.data.template = new Card().palette()
			that.data.qrcode = res.data.qrcode
			that.data.template.views[1].url = that.data.headimgurl
			that.data.template.views[2].text = that.data.username
			that.data.template.views[4].url = that.data.qrcode
			that.setData({
				template: that.data.template,
				show: true
			});
			wx.hideLoading()
		})
		
	},
	Next() {
		var now_index = this.data.now_index
		var list = this.data.list
		now_index++
		wx.showLoading({
			title: '加载中...',
			mask: true,
		})
		setTimeout(() => {
			if (now_index < list.length) {
				this.data.template.views[0].url = this.data.list[now_index]
				this.setData({
					now_index: now_index,
					template: this.data.template,
				})
			} else {
				this.data.template.views[0].url = this.data.list[0]
				this.setData({
					now_index: 0,
					template: this.data.template,
				})
			}
			wx.hideLoading({
				complete: (res) => {},
			})
		}, 1000);

	},

	save() {
		var that = this
		//图片保存到本地
		wx.saveImageToPhotosAlbum({
			filePath: that.imagePath,
			success: function (data) {
				wx.showModal({
					title: '已保存至相册',
					content: '发到朋友圈邀请好友留言吧~',
					showCancel: false,
				})
			},
			fail: function (err) {
				if (err.errMsg === "saveImageToPhotosAlbum:fail:auth denied" || err.errMsg === "saveImageToPhotosAlbum:fail auth deny" || err.errMsg === "saveImageToPhotosAlbum:fail authorize no response") {
					// 这边微信做过调整，必须要在按钮中触发，因此需要在弹框回调中进行调用
					wx.showModal({
						title: '提示',
						content: '需要您授权保存相册',
						showCancel: false,
						success: modalSuccess => {
							wx.openSetting({
								success(settingdata) {

									if (settingdata.authSetting['scope.writePhotosAlbum']) {
										wx.showModal({
											title: '获取权限成功',
											content: '再次点击保存图片即可保存~',
											showCancel: false,
										})
									} else {
										wx.showModal({
											title: '提示',
											content: '获取权限失败，将无法保存到相册哦~',
											showCancel: false,
										})
									}
								},
								fail(failData) {
									console.log("failData", failData)
								},
								complete(finishData) {
									console.log("finishData", finishData)
								}
							})
						}
					})
				}
			},

		})
	},

})