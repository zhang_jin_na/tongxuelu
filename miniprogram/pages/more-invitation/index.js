//index.js
const app = getApp()

Page({
	data: {
		img_src: ''
	},
	// 上传头像
	uploading: function (e) {
		var that = this
		wx.chooseImage({
			count: 1,
			sizeType: ['original', 'compressed'],
			sourceType: ['album', 'camera'],
			success(res) { 
				that.setData({
					img_src: res.tempFilePaths,
				})
			},
			fail(err) {}
		})
	},

	onShareAppMessage: function (e) {
		return {
			title: "快来一起拍毕业照吧",
			path: "/pages/more-invitation/main",
			imageUrl: this.data.img_src,
			success: function (res) {}
		};
	}
})