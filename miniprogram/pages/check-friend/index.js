const innerAudioContext = wx.createInnerAudioContext() //播放录音
var util = require('../../util.js');
const app = getApp()
Page({
	data: {
		user_info: {},
		show_login: true,
		receiveIndex: 0,
		receiveData: [],
		shareUid: '',
		nowData: [],
		is_show: false,
		page: 1,
		// receiveIndex: 17
	},
	onLoad: function (options) {
		var that = this
		// 传过来的share_id
		if (decodeURIComponent(options.scene)) { //海报分享进来
			this.data.share_id = decodeURIComponent(options.scene)
		}
		if (this.data.share_id == 'undefined') { //从首页右上角分享进来
			this.data.share_id = options.share_id
			// this.data.share_id =83870
		}
		var url = 'Member/showname?id=' + this.data.share_id
		app.get_request(url).then(function (result) {
			that.setData({
				share_name: result.data.data.list[0].username,
				headimgurl:result.data.data.list[0].headimgurl
			})
		})
	},
	onShow: function (e) {
		var that=this
		this.getFromInfo()
		app.wx_getWhereuserinfo().then(function (res) {
			if (res != 'new') {
				that.is_userInfo()
			}else{
				that.setData({
					show_login: true,
					is_show: true,
				})
			}
		})
	},

	is_userInfo(e) {
		const userInfo = wx.getStorageSync('user_info')
		// 如果来访的已登录过
		if (userInfo) {
			this.setData({
				user_info: userInfo,
				show_login: false, //将登录按钮设为false
			})
			this.postRecord()
			app.wx_uplogin()
		} else {
			this.setData({
				show_login: true,
			})
		}
	},
	// 授权按钮
	onGotUserInfo: function (e) {
		var that = this
		var state = e.detail.errMsg
		if (state == 'getUserInfo:ok') {
			app.wx_loginTip()
			app.wx_login().then(function (result) {
				app.wx_getUserInfo().then(function (result) {
					that.is_userInfo()
					app.wx_loginOk()
				})
			})
		} else {
			console.log("取消授权")
		}
	},
	// 获取分享人的ID的收到信息
	getFromInfo(e) {
		var that = this
		var share_id = that.data.share_id
		var page = that.data.page
		if (page == 1) {
			that.data.receiveData = []
		}
		var url = 'Mailbox/index?member_id=' + share_id
		app.get_request(url).then(function (res) {
			that.setData({
				count: res.data.count,
			})
			// 0是公开
			var urls = 'Mailbox/index?member_id=' + share_id + '&page=' + page + '&type=' + 0
			app.get_request(urls).then(function (result) {
				var receiveData = result.data.data.list
				for (var i = 0; i < receiveData.length; i++) {
					receiveData[i].create_time = util.formatTimeTwo(receiveData[i].create_time, 'Y/M/D')
					if (receiveData[i].taping) {
						var min = Math.floor(receiveData[i].record_time / 60) % 60
						var sec = receiveData[i].record_time % 60
						var receive_time_min = ('0' + min + ':' + (sec < 10 ? '0' + sec : sec))
						receiveData[i].record_time = receive_time_min
					}
				}
				if (e) {
					that.data.receiveData = that.data.receiveData.concat(receiveData)
				} else {
					if (page == 1) {
						that.data.receiveData = receiveData
					}
				}
				that.setData({
					letter_img: app.data.letter_img,
					receiveData: that.data.receiveData,
					nowData: that.data.nowData,
					is_show: true,
				})
			})
		})
	},

	postRecord() {
		var that = this
		// that.data.share_id
		var only_id = that.data.user_info.id + that.data.share_id
		var data = {
			member_id: that.data.share_id,
			user_id: that.data.user_info.id,
			only_id: only_id
		}
		app.post_request('Record/add', data).then(function (res) {
			if (res.data.status == 201) {
				var datas = {
					only_id: only_id,
					pv: 1
				}
				app.post_request('Record/pv', datas).then(function (res) {})
			}
		})
	},

	receiveChange(e) {
		var receiveIndex = e.detail.current
		var i = this.data.page * 20
		this.setData({
			receiveIndex: receiveIndex,
		})
		if (receiveIndex < (i - 1)) {
			return
		}
		this.data.page = this.data.page + 1
		this.getFromInfo(true)
	},

	// 在播放录音
	video: function (e) {
		var that = this
		that.setData({
			start_video: !that.data.start_video,
			play_time_min: "00:00"
		})
		innerAudioContext.src = this.data.taping_url
		console.log("play---taping_url:", this.data.taping_url)
		if (that.data.start_video) {
			innerAudioContext.play()
			that.data.play_inerval = null
			var time = 0
			that.data.play_inerval = setInterval(() => {
				time++
				if (time <= that.data.taping_time) {
					var min = Math.floor(time / 60) % 60
					var sec = time % 60
					that.setData({
						play_time_min: ('0' + min + ':' + (sec < 10 ? '0' + sec : sec))
					})
					if (time == that.data.taping_time) {
						console.log("播放时长到")
						clearInterval(that.data.play_inerval)
						innerAudioContext.stop()
						that.setData({
							start_video: false,
						})
					}
				}
			}, 1000);
		} else {
			// 停止播放
			var that = this
			clearInterval(this.data.play_inerval)
			console.log("点击了停止播放")
			innerAudioContext.stop()
			innerAudioContext.onStop((e) => {})
		}
	},
	// 详情
	toDetail: function (e) {
		var kind = 'check'
		var id = e.currentTarget.dataset.id
		wx.navigateTo({
			url: '/pages/detail/index?id=' + id + '&kind=' + kind ,
		})
	},
	// 生成
	toCreate() {
		wx.switchTab({
			url: '/pages/index/index',
		})
	},

	// 去给好友留言
	toLeave() {
		wx.navigateTo({
			url: '/pages/check-leave/index?member_id=' + this.data.share_id+ '&share_name=' + this.data.share_name +'&headimgurl='+this.data.headimgurl,
		})
	}
})