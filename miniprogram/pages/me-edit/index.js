var util = require('../../util.js');
const app = getApp()

Page({
	data: {
		user_src: '',
		year_arr: [],
		sex_arr: ['男', '女'],
		upload_img: '',
		show_name_pop: false, //修改姓名pop
		show_school_pop: false,
		schoolList: [],
	},
	onLoad() {},
	onShow: function (e) {
		wx.showLoading({
		  title: '数据加载中',
		  mask:true
		})
		this.setData({
			is_show:false
		})
		setTimeout(() => {
			this.getSchoolList()
					
		const userInfo = wx.getStorageSync('user_info')
		this.setData({
			id: userInfo.id,
			headimgurl: userInfo.headimgurl,
			name: userInfo.name,
			year: userInfo.year,
			sex: userInfo.sex,
			birthday: userInfo.birthday,
			mobile: userInfo.mobile,
			is_show:true
		})
		// 
		if (this.data.mobile) {
			this.setData({
				show_phone: false,
				phone: this.data.mobile
			})
		} else {
			this.setData({
				show_phone: true
			})
		}
		}, 100);
		// console.log("init_data:", userInfo)
		
	},
	getSchoolList() {
		var that = this
		var url = 'UserSchool/index'
		app.school_request(url).then(function (result) {
			wx.hideLoading()
			if (result.count != 0) {
				var school= result.list[0].school
			}
			that.setData({
				school: (school?school:'')
			})
		})
	},

	//编辑头像
	editImg: function () {
		var that = this
		wx.chooseImage({
			count: 1,
			sizeType: ['original', 'compressed'],
			sourceType: ['album', 'camera'],
			success(res) {
				app.wx_uploadFile('image', res.tempFilePaths[0]).then(function (res) {
					that.setData({
						headimgurl: JSON.parse(res.data).data,
					})
				})
			},
		})
	},
	// name
	nameChange() {
		this.setData({
			show_name_pop: true
		})
	},
	nameOk() {
		var name = this.data.name
		this.setData({
			name: name,
			show_name_pop: false,
		})
	},
	nameClose() {
		this.setData({
			show_name_pop: false,
			show_phone_pop: false
		})
	},
	bindNameInput(e) {
		this.data.name = e.detail.value
	},
	bindPhoneInput(e) {
		this.data.phone = e.detail.value
	},
	// school
	schoolChange() {
		wx.navigateTo({
			url: '/pages/me-school/school',
		})

	},
	phoneChange() {
		this.setData({
			show_phone_pop: true
		})
	},
	phoneOk() {
		var phone = this.data.phone
		this.setData({
			phone: phone,
			show_phone_pop: false
		})
	},
	// 性别
	sexChange(e) {
		var sex = this.data.sex_arr[e.detail.value]
		this.setData({
			sex: (sex == '女' ? '0' : '1')
		})
	},
	// 生日
	DateChange(e) {
		this.setData({
			birthday: e.detail.value
		})
	},
	// 电话
	userPhone(e) {
		var that = this;
		var iv = e.detail.iv
		var encryptedData = e.detail.encryptedData
		wx.login({
			success(res) {
				var code = res.code //存取第三方平台code到公用数据字段
				wx.request({
					method: 'post',
					url: 'https://classmate.lodidc.cn/api/Member/getUserSession',
					data: {
						'code': code,
						'encryptedData': encryptedData,
						'iv': iv
					},
					success(res) {
						var session_key = res.data.session_key
						wx.request({
							method: 'post',
							url: 'https://classmate.lodidc.cn/api/Member/getUserPhone',
							data: {
								'code': code,
								'encryptedData': encryptedData,
								'iv': iv,
								'session_key': res.data.session_key,
							},
							success(res) {
								if (res.data == '-41003') {
									wx.request({
										method: 'post',
										url: 'https://classmate.lodidc.cn/api/Member/getUserSession',
										data: {
											'code': code,
											'encryptedData': encryptedData,
											'iv': iv
										},
										success(res) {
											// var session_key=res.data.session_key
											wx.request({
												method: 'post',
												url: 'https://classmate.lodidc.cn/api/Member/getUserPhone',
												data: {
													'code': code,
													'encryptedData': encryptedData,
													'iv': iv,
													'session_key': session_key,
												},
												success(res) {
													console.log('获取电话号码22', res)
													if (res.data.phoneNumber) {
														var phone = res.data.phoneNumber
														// wx.setStorageSync('phone', phone)
														that.setData({
															phone: phone,
															show_phone: false,
														})
													}
												}
											})
										}
									})
								} else {
									console.log('获取电话号码111', res)
									if (res.data.phoneNumber) {
										var phone = res.data.phoneNumber
										// wx.setStorageSync('phone', phone)
										that.setData({
											phone: phone,
											show_phone: false,
										})
									}
								}

							}
						})
					}
				})
			}
		})

	},

	//保存
	save: function () {
		let that = this;
		var url = 'Member/update'
		var data = {
			"id": this.data.id,
			"headimgurl": this.data.headimgurl,
			"name": this.data.name,
			// "school": this.data.school,
			"year": this.data.year,
			"sex": this.data.sex,
			"birthday": this.data.birthday,
			"mobile": this.data.phone,
		}
		app.post_request(url, data).then(function (result) {
			wx.showToast({
				title: '保存成功',
				duration: 2000,
				success: function () {
					setTimeout(() => {
						wx.navigateBack({
							delta: 1
						})
					}, 2000);
				},
			})
		})
	},

	back() {
		wx.navigateBack({
			delta: 1
		})
	}
})