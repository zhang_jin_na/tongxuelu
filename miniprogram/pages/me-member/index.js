var util = require('../../util.js');
const app = getApp()

Page({
  data: {
    user_info: '',
    pay_money: '',
    huishou: '',
    show_pop: false,
    is_ios: false,
    vip_test_time: '',
  },
  onLoad(e) {

  },
  onShow() {
    wx.showToast({
      title: '数据加载中...',
      icon: 'loading',
      mask: true,
    })
    this.is_userInfo()
  },
  is_userInfo() {
    var that = this
    app.get_info().then(function (res) {
      var user_info = res.data.data.list[0]
      if (user_info.vip_time == 1) {
        that.data.show_text = true
        that.data.is_Vip = true
      } else {
        that.data.vip_test_time = util.formatTimeTwo(user_info.vip_time, 'YMDhm')
        user_info.vip_time = util.formatTimeTwo(user_info.vip_time, 'Y/M/D h:m')
        that.Time() //判断vip过期时间
      }
      
      that.setData({
        show_text: that.data.show_text,
        is_Vip: that.data.is_Vip,
        user_info: user_info,
        
      })
      setTimeout(() => {
        that.setData({
          is_show: true
        })
        wx.hideToast()
      }, 900);
     
      wx.setStorageSync('user_info', that.data.user_info)
      // console.log("实时获取:", that.data.user_info)
    })
  },
  Time() {
    var that = this
    var time = that.data.vip_test_time
    that.data.time_inerval = []
    that.data.time_inerval = setInterval(() => {
      var now_time = util.vipTime(new Date())
      if (now_time >= time) {
        that.setData({
          is_Vip: false
        })
        clearInterval(that.data.time_inerval)
      } else {
        that.setData({
          is_Vip: true
        })
      }
    }, 1000);
  },
  Pay() {
    this.getPayMoney()
  },
  getPayMoney() { //获取会员金额
    var that = this
    app.post_request('Money/index').then(function(res){
      var money = that.data.user_info.money
      var all_money = res.data.money
      //如果后台金额大于等于支付的会员金额
      if (Number(money) >= all_money) {
        that.data.pay_money = 0 //只需支付0元
        that.data.huishou = all_money //回收的金额 
        that.data.pay_0 = true
      } else {
        that.data.pay_0 = false
        that.data.pay_money = (all_money - money).toFixed(2)
        that.data.huishou = money
      }
      that.setData({
        money: money,
        pay_0: that.data.pay_0,
        discount: res.data.discount,
        all_money: Number(all_money),
        pay_money: that.data.pay_money,
        show_pop: true,
      })
    })
  },

  toPay() {
    var that = this
    if (that.data.pay_0) { //支付0
      that.addvip()
    } else {
      wx.getSystemInfo({
        success(res) {
          if (res.platform == "ios") {
            // app.wx_modalTip('目前ios系统不能付费', '请联系客服哦~')
            that.setData({
              is_ios: true,
              show_pop: false,
            })
          } else {
            that.payPay()
          }
        }
      })
    }
  },

  payPay() {
    var that = this
    wx.request({
      method: 'post',
      url: 'https://classmate.lodidc.cn/api/Order/add',
      data: {
        // that.data.red_packet
        total_fee: that.data.pay_money,
        member_id: that.data.user_info.id,
      },
      success(res) {
        console.log("pay-ing", res)
        var order_id = res.data.order_id
        var amount = res.data.amount
        wx.login({
          success(res) {
            var code = res.code //存取第三方平台code到公用数据字段
            wx.request({
              method: 'post',
              url: 'https://classmate.lodidc.cn/api/Order/orderpay',
              data: {
                code: code,
                amount: amount,
                order_id: order_id
              },
              success(res) {
                wx.requestPayment({
                  appId: res.data.data.appId,
                  nonceStr: res.data.data.nonceStr, //随机字符串
                  package: res.data.data.package, //统一下单接口返回的 prepay_id 参数值，提交格式如：prepay_id=***
                  paySign: res.data.data.paySign, //签名
                  signType: 'MD5', //签名算法
                  timeStamp: res.data.data.timestamp, //时间戳
                  success: function (res) {
                    app.wx_showSuccess("开通成功")
                    that.setData({
                      show_text: true,
                      show_pop: false,
                    })
                    that.upVip() //更新vip状态
                  },
                  fail: function (res) {},
                })
              }
            })
          },
          fail: function (err) {
            console.log("app.login.fail", err);
          },
        })
      }
    })

  },
  addvip() {
    var that = this
    var data={
      member_id: that.data.user_info.id,
      money: that.data.pay_money,
    }
    app.post_request('Member/addvip',data).then(function(res){
      that.upVip() //更新vip状态
      app.wx_showSuccess("开通成功")
      that.setData({
        show_text: true,
        show_pop: false,
      })
    })
  },
  upVip() {
    var that = this
    app.post_request('Member/upvip',{
      id: that.data.user_info.id,
    }).then(function(res){
      console.log("upVip", res)
        that.is_userInfo()
    })
    app.post_request('Member/withdrawal', {
      id: that.data.user_info.id,
      money: that.data.huishou
    }).then(function(res){
      console.log("更新money值", res)
    })
  },
  close() {
    this.setData({
      show_pop: false,
      is_ios: false,
    })
  },
  onUnload() {
    clearInterval(this.data.time_inerval)
  },
})