//index.js
const app = getApp()

Page({
  data: {
    modalName: '',
    is_show:false
  },
  onLoad(){
    app.createAdv()
  },
  onShow() {
    var that =this
    app.showAdv()
    app.wx_getWhereuserinfo().then(function (res) {
			if (res != 'new') {
        that.is_userInfo()
			}else{
				that.setData({
          show_login: true,
          is_show:true
				})
			}
		})
  },
  is_userInfo(e) {
    const user_info = wx.getStorageSync('user_info')
    
    if (user_info) {
      this.setData({
        show_login: false,
        user_info:user_info,
        is_show:true
      })
    } else {
      this.setData({
        show_login: true,
      })
    }
    app.wx_uplogin()
  },
  toCap: function () {
    wx.navigateTo({
      url: '/pages/more-cap/index',
    })
  },
  hideModal() {
    this.setData({
      modalName: ''
    })
  },
  toInvitation: function (e) {
    this.setData({
      modalName: e.currentTarget.dataset.target
    })
  },

  haibao() {
    const user_info = wx.getStorageSync('user_info')
    wx.requestSubscribeMessage({
			tmplIds: ['d1KQ36Nqy5uXsQahV1Ri3qORwYl562FwO1cK0mE9hDQ',
				'f9ANDdl3vReN2pk2EL4Qw8F3WlIJwPTmBgDgg4pTcrQ'
			],
      success(res) {
        // console.log(res)
        if (user_info) {
          wx.showLoading({
					  title: '跳转中...',
					  mask:true,
					  duration:1500,
					})
					setTimeout(() => {
						wx.navigateTo({
							url: '/pages/Haibao/haibao',
						})
					}, 1500);
        }
      },
      fail(err) {
        console.log(err, '订阅err')
      }
    })
  },
  //授权
  onGotUserInfo: function (e) {
    var that = this
    var state = e.detail.errMsg
    if (state == 'getUserInfo:ok') {
      app.wx_loginTip()
      var that = this
      app.wx_login().then(function (result) {
        app.wx_getUserInfo().then(function (result) {
          that.is_userInfo()
          app.wx_loginOk()
        })
      })
    } else {
      console.log("取消授权")
    }
  },
})