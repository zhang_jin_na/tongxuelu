var util = require('../../util.js');
const app = getApp()
Page({

   data: {
      is_add: false,
      year_arr: [],
      education_arr: ['小学', '初中', '中专/中技', '高中', '大专', '本科', '硕士', '博士'],
      show_major:true,
      yearArray: [],
      infoList:[],
   },

   onLoad: function (options) {
      this.yearArr()
   },

   onShow: function () {
      this.getSchoolList()
   },
   getSchoolList(){
      var that =this
      var url='UserSchool/index'
		app.school_request(url).then(function(result){
			that.setData({
            infoList:result.list
         })
		})
   },
   yearArr() {
      var end_year = util.yearTime(new Date())
      var year_arr = this.data.year_arr
      for (var i = 2000; i <= end_year; i++) {
         year_arr.push(i)
      }
      this.data.yearArray[0] = year_arr
      this.data.yearArray[1] = year_arr
      this.setData({
         yearArray: this.data.yearArray
      })
   },
   schoolInput(e){  // 学校输入框
      var that =this
      this.setData({
         school:e.detail.value,
      })
      if(e.detail.value!=''){
         this.getSchool()
         
      }else{
         that.setData({
            is_tip:false,
         })
      }
   },
   select_close(){
      this.setData({
         is_tip:false,
      })
   },
   select_check(){
      this.setData({
         is_tip:false,

      })
   },
   getSchool(){  //搜索学校
      var that =this
      var url='School/index?school=' + this.data.school
      app.get_request(url).then(function(result){
         if(result.data.data.list!=0){
            that.data.tip_school=result.data.data.list
            that.data.is_tip=true
         }else{
            that.data.tip_school=[]
            that.data.is_tip=false
         }
         that.setData({
            tip_school:that.data.tip_school,
            is_tip:that.data.is_tip,
         })
      })
   },
   selectSchool(e){
      var name =e.currentTarget.dataset.item
      this.setData({
         school:name,
         is_tip:false,
      })
   },
   majorInput(e){
      this.setData({
         major:e.detail.value
      })
   },
   Change1(e){  //学历的选择
      var edu_index=e.detail.value
      if(edu_index==0||edu_index==1||edu_index==3){
         this.data.show_major=false
         this.data.major=''
      }else{
         this.data.show_major=true
         this.data.major=''
      }
      this.setData({
         edu_index:edu_index,
         major:this.data.major,
         show_major:this.data.show_major,
         education:this.data.education_arr[e.detail.value]
      })
   },
   Change2(e) {
      var indexArr = e.detail.value
      var yearArray=this.data.yearArray
      this.setData({
         indexArr: indexArr,
         year:yearArray[0][indexArr[0]]+'-'+yearArray[1][indexArr[1]]
      })
   },
   toAdd() {
      this.setData({
         is_add: true
      })
   },
   cancel(){
      this.setData({
         is_add:false,
         show_major:true,
         school:'',
         education:'',
         major:'',
         year:'',
      })
   },
   add(){
      if(!this.data.school){
         app.wx_modalTip('提示','请填写学校名称')
      }else if(!this.data.education){
         app.wx_modalTip('提示','请选择学历')
      }else if(this.data.show_major){
         if(!this.data.major){
            app.wx_modalTip('提示','请填写主修专业')
         }else if(!this.data.indexArr){
            app.wx_modalTip('提示','请选择时间')
         }else{
            this.Add()
            return
         }
      }else if(!this.data.indexArr){
         app.wx_modalTip('提示','请选择时间')
      }else{
         this.Add()
      }
   },
   Add(){
      var that=this
      var user_info=wx.getStorageSync('user_info')
      var infoList={
         id:user_info.id,
         school:this.data.school,
         major:this.data.major,
         education:this.data.education,
         edu_index:this.data.edu_index,
         year:this.data.year
      }
      app.post_request('UserSchool/add',infoList).then(function(result){
         that.getSchoolList()
         wx.showToast({
            title: '添加成功',
            mask:true,
            duration:1000,
          })
          setTimeout(() => {
            that.cancel()
             that.setData({
                infoList:that.data.infoList
             })
          }, 1000);
      })
   },
   toEdit(e){
      var index = e.currentTarget.dataset.index
      var nowData = this.data.infoList[index]
      this.setData({
         show_edit:true,
         is_add:true,
         school:nowData.school,
         education:nowData.education,
         major:nowData.major,
         year:nowData.year,
      })
   },
   del(e) {
      var that=this
      var index = e.currentTarget.dataset.index
      var school_id=this.data.infoList[index].userschool_id
      var user_info=wx.getStorageSync('user_info')
      var data={
         userschool_ids:school_id
      }
      app.post_request('UserSchool/delete',data).then(function(res){
         that.getSchoolList()
      })
   },
})