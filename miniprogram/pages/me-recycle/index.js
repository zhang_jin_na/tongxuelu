var util = require('../../util.js');
const app = getApp()

Page({
   data: {
      userInfo: '',
      Listids: [],
      topKind: ['我收到的', '我发出的'],
      show_kind: false, //false=我收到的,true=我发送的
      kind_index: 0,
      page: 1,
      reData: [],
      sendData: [],
   },
   onLoad: function (e) {
      this.data.id = e.id || 83870
   },
   onShow: function () {
      this.setData({
         page: 1,
         more: true
      })
      this.getRecycle(this.data.kind_index)
   },
   selectKind() {
      this.setData({
         show_kind: !this.data.show_kind
      });
   },
   // 点击kind下拉列表 
   optionTap(e) {
      let index = e.currentTarget.dataset.index; //获取点击的下拉列表的下标 
      this.setData({
         kind_index: index,
         show_kind: !this.data.show_kind,
         page: 1,
         sendData: [],
         reData: [],
         more: true
      });
      wx.showLoading({
         title: '数据加载中...',
      })
      this.getRecycle(this.data.kind_index)
   },

   selectItem(e) {
      var index = e.currentTarget.dataset.index
      var id = e.currentTarget.dataset.id
      var Listids = this.data.Listids
      if (Listids[index]) {
         Listids[index] = false
      } else {
         Listids[index] = id
      }
      this.setData({
         Listids: Listids,
         index: index,
      })
   },
   recycle() {
      var that = this
      var Listids = this.data.Listids
      if (Listids.length == 0) {
         wx.showModal({
            title: '提示',
            content: '请先勾选需要还原的记录',
            showCancel: false,
         })
         return
      }
      var length = 0
      for (let i = 0; i < Listids.length; i++) {
         if (!Listids[i]) {
            length++
         }
      }
      if (length != Listids.length) {
         wx.showModal({
            title: '提示',
            content: '确定还原这些记录么？',
            success(res) {
               if (res.confirm) {
                  wx.showLoading({
                     title: '还原中...',
                  })
                  that.toRecycle()
               }
            }
         })
      } else {
         wx.showModal({
            title: '提示',
            content: '请先勾选需要还原的记录',
            showCancel: false,
         })
      }
   },
   toRecycle() {
      var that = this
      var Listids = this.data.Listids
      var newListids = []
      for (let i = 0; i < Listids.length; i++) {
         const element = Listids[i]
         if (element) {
            newListids.push(element)
         }
      }
      let ids = newListids.join(',')
      app.post_request('Mailbox/renew', {
         id: ids
      }).then(function (res) {
         wx.hideLoading()
         that.setData({
            Listids: []
         })

         setTimeout(() => {
            that.getRecycle(that.data.kind_index, true)
         }, 1000);
      })
   },
   toDetail(e) { //目前还没有
      var id = e.currentTarget.dataset.id
      var kind = ''
      if (this.data.kind_index == 0) {
         kind = 'receive'
      } else {
         kind = 'send'
      }
      wx.navigateTo({
         url: '/pages/detail/index?id=' + id + '&kind=' + kind,
      })
   },

   getRecycle(options, recycle) {
      var that = this
      var page = that.data.page
      if (page == 1) {
         that.data.sendData = []
         that.data.dataList = []
      }
      // 点击我收到
      if (options == 0) {
         var url1 = '/Mailbox/index?member_id=' + that.data.id + '&del=' + '1' + "&page=" + page
         app.get_request(url1).then(function (res) {
            var data = res.data.data.list
            var count = res.data.count
            for (var i = 0; i < data.length; i++) {
               data[i].create_time = util.formatTimeTwo(data[i].create_time, 'Y.M.D')
            }
            if (!recycle) {
               that.data.dataList = that.data.dataList.concat(data)
            } else {
               if (page == 1) {
                  console.log("re", data)
                  that.data.dataList = data
               } else {
                  that.data.dataList.splice(that.data.index, 1)
               }
               wx.showToast({
                  title: '还原成功',
                  duration: 1500,
               })
            }
            // that.data.reData = that.data.reData.concat(data)
            // 我收到的
            if (data.length < 20 && page > 0) {
               that.data.more = false
            }
            that.setData({
               reData: that.data.reData,
               dataList: that.data.dataList,
               count: count,
               more: that.data.more,
               re_show: true,
            })
            wx.hideLoading()
         })
      }
      // 点击我发出
      if (options == 1) {
         var url2 = 'Mailbox/index?from_id=' + that.data.id + '&del=' + '1' + "&page=" + page
         app.get_request(url2).then(function (res) {
            var data = res.data.data.list
            var count = res.data.count
            for (var i = 0; i < data.length; i++) {
               data[i].create_time = util.formatTimeTwo(data[i].create_time, 'Y.M.D')
            }
            if (!recycle) {
               that.data.dataList = that.data.dataList.concat(data)
            } else {
               if (page == 1) {
                  console.log("send", data)
                  that.data.dataList = data
               } else {
                  that.data.dataList.splice(that.data.index, 1)
               }
               wx.showToast({
                  title: '还原成功',
                  duration: 1500,
               })
            }
            if (data.length < 20 && page > 0) {
               that.data.more = false
            }
            that.setData({
               // sendData: that.data.sendData,
               dataList: that.data.dataList,
               count: count,
               more: that.data.more,
               re_show: false,
            })
            wx.hideLoading()
         })
      }
   },

   onReachBottom() {
      if (!this.data.more) {
         return
      }
      this.data.page = this.data.page + 1
      setTimeout(() => {
         this.getRecycle(this.data.kind_index)
      }, 1000)
   },
})