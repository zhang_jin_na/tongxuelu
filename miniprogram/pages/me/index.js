var util = require('../../util.js');
const app = getApp()
Page({
	data: {
		user_info: {},
		token: '',
		now_time: '',
		time_inerval: '',
		vip_test_time: '',
		huishou: '',
	},
	onLoad() {
		wx.showLoading({
			title: '数据加载中...',
			mask: true
		})
	},
	onShow: function () {
		var that = this
		app.wx_getWhereuserinfo().then(function (res) {
			if (res != 'new') {
				that.is_userInfo('init')
			}else{
				that.setData({
					show_login: true,
					is_show: true,
				})
				wx.hideLoading()
			}
		})
	},
	is_userInfo(e) {
		var that = this
		const user_infos = wx.getStorageSync('user_info')
		if (user_infos) {
			app.get_info().then(function (res) {
				var user_info = res.data.data.list[0]
				if (user_info.vip_time == 1) { //永久会员
					that.setData({
						show_yongjiu: true,
						is_Vip: true,
					})
				} else {
					var vip_time = util.formatTimeTwo(user_info.vip_time, 'Y.M.D h:m')
					that.data.vip_test_time = util.formatTimeTwo(user_info.vip_time, 'YMDhm')
					that.Time() //判断vip过期时间
				}
				app.school_request('UserSchool/index').then(function (result) {
					if (result.count != 0) {
						var school = result.list[0].school
					}
					that.setData({
						show_login: false,
						user_info: user_info,
						vip_time: vip_time,
						all: Number(user_info.money),
						year: user_info.year,
						school: (school ? school : ''),
						is_show: true,
					})

					that.recordSum()
					wx.setStorageSync('user_info', that.data.user_info)
					if(e){
						wx.hideLoading()
					}
				})
			})
			app.wx_uplogin()
		} else {
			// 执行登录操作
			that.setData({
				show_login: true,
				is_show: true,
			})
		}
	},
	Time() {
		var that = this
		var time = that.data.vip_test_time
		// var time=202005061654
		that.data.time_inerval = []
		that.data.time_inerval = setInterval(() => {
			var now_time = util.vipTime(new Date())
			if (now_time >= time) {
				clearInterval(that.data.time_inerval)
				console.log("会员已过期")
				that.setData({
					is_Vip: false
				})
			} else {
				that.setData({
					is_Vip: true
				})
			}
		}, 1000);
	},
	// 授权按钮
	onGotUserInfo: function (e) {
		var state = e.detail.errMsg
		if (state == 'getUserInfo:ok') {
			app.wx_loginTip()
			var that = this
			app.wx_login().then(function (result) {
				app.wx_getUserInfo().then(function (result) {
					that.is_userInfo()
					app.wx_loginOk()
				})
			})
		} else {
			console.log("取消授权")
		}
	},
	// 会员
	toMember: function () {
		wx.navigateTo({
			url: '/pages/me-member/index'
		})
	},
	toEdit: function () {
		wx.navigateTo({
			url: '/pages/me-edit/index',
		})
	}, //

	recordSum() {
		var that = this
		var member_id = that.data.user_info.id
		var url = 'Record/index?member_id=' + member_id
		app.get_request(url).then(function (res) {
			var record_sum = res.data.data.count
			that.setData({
				record_sum: record_sum || '0'
			})
		})
	},
	// 访客记录
	toRecord() {
		if (this.data.show_yongjiu) {
			wx.navigateTo({
				url: '/pages/me-record/index?id=' + this.data.user_info.id,
			})
		} else {
			var that = this
			wx.showModal({
				title: '温馨提示',
				content: '开通永久会员可以访问访客记录哦',
				confirmText: '开通会员',
				success(res) {
					if (res.confirm) {
						that.setData({
							show_pop: true,
							is_record: true,
						})
						that.getPayMoney()
						wx.hideTabBar()
					}
				}
			})
		}
	},
	//获取后台会员金额
	getPayMoney() {
		var that = this
		wx.request({
			method: 'post',
			url: 'https://classmate.lodidc.cn/api/Money/index',
			success(res) {
				var money = that.data.user_info.money
				var all_money = res.data.money
				if (Number(money) >= all_money) {
					that.data.pay_money = 0 //只需支付0元
					that.data.huishou = all_money //回收的金额 
					that.data.pay_0 = true
				} else {
					that.data.pay_0 = false
					that.data.pay_money = (all_money - money).toFixed(2)
					that.data.huishou = money
				}
				that.setData({
					money: money,
					pay_0: that.data.pay_0,
					discount: res.data.discount,
					all_money: Number(all_money),
					pay_money: that.data.pay_money
				})
			}
		})
	},
	addvip() { //支付0元
		var that = this
		var data = {
			member_id: that.data.user_info.id,
			money: that.data.huishou,
		}
		app.post_request('Member/addvip', data).then(function (res) {
			that.setTo() //支付成功后跳转到记录或回收
			that.upVip() //更新vip状态
		})
	},
	close() {
		this.setData({
			show_pop: false,
			is_ios: false,
			is_record: false,
		})
		wx.showTabBar()
	},
	testPay() { // 确认开通按钮
		var that = this
		if (that.data.pay_0) { //支付0元
			that.addvip()
		} else {
			wx.getSystemInfo({
				success: function (res) {
					if (res.platform == "ios") {
						// app.wx_modalTip('目前ios系统不能付费', '请联系客服哦~')
						wx.showTabBar()
						that.setData({
							is_ios: true,
							show_pop: false,
						})
					} else {
						that.toPay()
					}
				}
			})
		}
	},
	toPay() {
		var that = this
		wx.request({
			method: 'post',
			url: 'https://classmate.lodidc.cn/api/Order/add',
			data: {
				total_fee: that.data.pay_money,
				member_id: that.data.user_info.id,
			},
			success(res) {
				console.log("resres", res)
				var order_id = res.data.order_id
				var amount = res.data.amount
				wx.login({
					success(res) {
						var code = res.code //存取第三方平台code到公用数据字段
						wx.request({
							method: 'post',
							url: 'https://classmate.lodidc.cn/api/Order/orderpay',
							data: {
								code: code,
								amount: amount,
								order_id: order_id
							},
							success(res) {
								wx.requestPayment({
									appId: res.data.data.appId,
									nonceStr: res.data.data.nonceStr, //随机字符串
									package: res.data.data.package, //统一下单接口返回的 prepay_id 参数值，提交格式如：prepay_id=***
									paySign: res.data.data.paySign, //签名
									signType: 'MD5', //签名算法
									timeStamp: res.data.data.timestamp, //时间戳
									success: function (res) {
										that.setTo() //支付成功后跳转到记录或回收
										that.upVip() //更新vip状态
										console.log("pay.ok", res);
									},
									fail: function (res) {
										// fail
										console.log("pay.fail", res);
									},
								})
							}
						})
					},
					fail: function (err) {
						console.log("app.login.fail", err);
					},
				})
			}
		})

	},
	//支付成功后跳转到记录或回收
	setTo() {
		var that = this
		that.setData({
			show_pop: false,
		})
		wx.showLoading({
			title: '跳转中...',
			mask: true,
		})
		setTimeout(() => {
			wx.hideLoading()
			if (that.data.is_record) {
				wx.navigateTo({
					url: '/pages/me-record/index?id=' + that.data.user_info.id,
				})
			} else {
				wx.navigateTo({
					url: '/pages/me-recycle/index?id=' + that.data.user_info.id,
				})
			}
		}, 1500);
	},
	upVip() {
		var that = this
		wx.request({
			method: 'post',
			url: 'https://classmate.lodidc.cn/api/Member/upvip',
			data: {
				id: that.data.user_info.id,
			},
			success(res) {
				console.log("upVip", res)
				that.is_userInfo()
			}
		})
		wx.request({
			method: 'post',
			url: 'https://classmate.lodidc.cn/api/Member/withdrawal',
			data: {
				id: that.data.user_info.id,
				money: that.data.huishou //抵扣掉的金额
			},
			success(res) {
				console.log("更新money值", res)
			}
		})
	},
	// 余额
	toMoney: function () {
		var money = this.data.user_info.money
		var id = this.data.user_info.id
		var is_Vip = this.data.show_yongjiu
		wx.navigateTo({
			url: '/pages/me-money/index?allMoney=' + money + '&id=' + id + '&is_Vip=' + is_Vip,
		})
	},
	toRecycle() {
		if (this.data.is_Vip) {
			wx.navigateTo({
				url: '/pages/me-recycle/index?id=' + this.data.user_info.id,
			})
		} else {
			var that = this
			wx.showModal({
				title: '温馨提示',
				content: '开通永久会员可以回收已删除的内容哦~',
				confirmText: '开通会员',
				success(res) {
					if (res.confirm) {
						that.setData({
							show_pop: true,
							is_record: false,
						})
						that.getPayMoney()
					}
				}
			})
		}
	},
	onHide: function () {
		clearInterval(this.data.time_inerval)
	},
	handleContact(e) {
		console.log(e.detail.path)
		console.log(e.detail.query)
	}
})