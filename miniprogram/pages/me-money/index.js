//index.js
const app = getApp()

Page({
  data: {
    userInfo: {},
    member_id: '',
    is_vip: '',
  },
  onLoad: function (e) {
    var allMoney = e.allMoney
    this.data.is_vip = e.is_Vip //永久会员标志
    this.data.member_id = e.id
    if (allMoney != undefined) {
      this.setData({
        allMoney: Number(allMoney)
      })
    } else {
      this.setData({
        allMoney: 0
      })
    }
    const user_info = wx.getStorageSync('user_info')
  },
  onShow(){
    this.show_ti_xian()
  },
  show_ti_xian(){ //判断余额是否为0
    var allMoney=this.data.allMoney
    if(allMoney>0){
      this.data.show_tixian=true
    }else{
      this.data.show_tixian=false
    }
    this.setData({
      show_tixian:this.data.show_tixian
    })
  },
  // 点击提現按钮
  ti_xian_init() {
    var that = this
    var allMoney = this.data.allMoney
    var is_vip=this.data.is_vip
    if(is_vip){ //如果是永久会员,免服务费
      this.data.pay_server=0
      this.data.act_money=allMoney
    }else{
      this.data.pay_server=2
      this.data.act_money=(allMoney - allMoney * 0.02).toFixed(2)
    }
    this.setData({
      show_pop: true,
      pay_server:this.data.pay_server,
      act_money: this.data.act_money
    })
  },
  ti_xian() {
    var allMoney = this.data.allMoney
    var is_vip = this.data.is_vip
    if (is_vip) { //如果是永久会员
      if (allMoney > 0) {
        wx.showLoading({
          title: '提现中...',
          mask: true,
        })
        setTimeout(() => {
          this.to_ti_xian()
        }, 1000);
      }
    } else { //不是会员、or体验会员
      if (allMoney >= 10) {
        wx.showLoading({
          title: '提现中...',
        })
        setTimeout(() => {
          this.to_ti_xian()
        }, 1000);
      } else {
        app.wx_modalTip('提示', '非会员满10元才可以提现哦')
      }
    }
  },
  to_ti_xian() {
    var that = this
    const token = wx.getStorageSync('token')
    wx.request({
      method: 'post',
      header: {
        Authorization: token
      },
      url: 'https://classmate.lodidc.cn/api/Withdrawal/add',
      data: {
        member_id: that.data.member_id,
        money: that.data.allMoney  //金额
        // money: 1,
        // member_id:83870
      },
      success(res) {
        wx.hideLoading()
        that.show_ti_xian()
        wx.showModal({
          title: '已提交申请',
          content: '我们将在2小时内通过微信红包的形式发放到您的账号',
          showCancel:false,
          success(res){
            if(res.confirm){
              wx.requestSubscribeMessage({
                tmplIds: [
                  'tSqFPsGaXie2f6SsvWxUuA8VBwooxCtZ-w_Y-raUyHc',
                ],
                success(res) {
                  console.log(res,"提现通知-ok")
                },
                fail(err) {
                  console.log(err, '提现通知err')
                }
              })
            }
          }
        })
        that.setData({
          allMoney: 0,
          show_pop: false,
        })
      }
    })
  },

  //红包记录
  toRecord: function () {
    wx.navigateTo({
      url: '/pages/me-money-record/index?member_id=' + this.data.member_id,
    })
  },
  close() {
    this.setData({
      show_pop: false,
    })
  },
  toMember() {
    wx.navigateTo({
      url: '/pages/me-member/index'
    })
  }
})