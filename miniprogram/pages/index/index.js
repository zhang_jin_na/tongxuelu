const innerAudioContext = wx.createInnerAudioContext() //播放录音
var util = require('../../util.js');
const app = getApp()
let letterData = {}
Page({
	data: {
		show_login: true,
		avatarUrl: '',
		sendIndex: 0,
		receiveIndex: 0,
		now_index: 0, //在播放的录音下标
		user_info: {},
		show_send: false, //我发出/收到
		show_top_sum: true, //头部收到/发出的数量状态
		start_video: false, //录音开始状态
		show_center_data: true,
		play_inerval: '',
		taping_url: '',
		taping_time: '',
		page: 1,
		topKind: ['我发出的', '我收到的'],
		kind_index: 1,
		sendDataList: [],
		// receiveIndex: 17,
		receiveDataList: [],
		is_init: false,

	},

	onLoad: function (e) {

	},
	onShow: function (e) {
		var that = this
		app.wx_getWhereuserinfo().then(function (res) {
			if (res != 'new') {
				const userInfo = wx.getStorageSync('user_info')
				that.is_userInfo(userInfo)
			} else {
				that.newUser()
				that.setData({
					show_login: true,
				})
			}
		})
	},
	is_userInfo(userInfo) {
		if (userInfo) {
			this.setData({
				user_info: userInfo,
				show_login: false,
			})
			this.getReceiveData()
			this.newUser(userInfo.id)
			this.getSendData()
			app.wx_uplogin()
		}
	},
	newUser(member_id) {
		var that = this
		var id = (member_id ? member_id : 0)
		app.post_request('Mailbox/official', {
			member_id: id
		}).then(function (res) {
			var letter = res.data.data.list[0]
			var suiji = res.data.suiji
			var Redrecord = res.data.Redrecord
			letter.create_time = util.formatTimeTwo(letter.create_time, 'Y/M/D')
			var min = Math.floor(letter.record_time / 60) % 60
			var sec = letter.record_time % 60
			letter.record_time = ('0' + min + ':' + (sec < 10 ? '0' + sec : sec))
			that.data.receiveDataList[0] = letter
			that.setData({
				isShow: true,
				suiji: (that.data.suiji ? that.data.suiji : suiji),
				letter_img: app.data.letter_img,
				receiveDataList: that.data.receiveDataList,
			})
			//用来做为第一次获取官方来信的随机金额判断，如存在则不需要更新随机金额
			wx.setStorageSync('suiji', that.data.suiji)
		})
	},
	// 获取数据
	getSendData(e) {
		var that = this
		var page = that.data.page
		if (page == 1) {
			that.data.sendDataList = []
		}
		// this.data.user_info.id
		var url = 'Mailbox/index?from_id=' + this.data.user_info.id + '&page=' + page + '&del=' + 0
		app.get_request(url).then(function (result) {
			var sendData = result.data.data.list
			var count = result.data.count
			for (var i = 0; i < sendData.length; i++) {
				sendData[i].create_time = util.formatTimeTwo(sendData[i].create_time, 'Y/M/D')
				if (sendData[i].taping) {
					var min = Math.floor(sendData[i].record_time / 60) % 60
					var sec = sendData[i].record_time % 60
					var send_time_min = ('0' + min + ':' + (sec < 10 ? '0' + sec : sec))
					sendData[i].record_time = send_time_min
				}
			}
			if (e == 'page') { //分页
				that.data.sendDataList = that.data.sendDataList.concat(sendData)
			} else {
				if (page == 1) {
					if (sendData.length <= 20) {
						that.data.sendDataList = sendData
					} else {}
				}
			}
			that.setData({
				isShow: true,
				sendDataList: that.data.sendDataList,
				send_count: count
			})
			if (e == 'Refresh') {
				wx.hideLoading()
			}
		})
	},
	getReceiveData(e) {
		var that = this
		var page = that.data.page
		if (page == 1) {
			that.data.receiveDataList = []
		}
		var vip_time = that.data.user_info.vip_time
		var time = (vip_time == 1 ? '99999999' : '2592000')
		// this.data.user_info.id
		var url = 'Mailbox/showindex?member_id=' + this.data.user_info.id + '&page=' + page + '&time=' + time + '&del=' + 0
		app.get_request(url).then(function (result) {
			var receiveData = result.data.data.list
			var count = result.data.count

			for (var i = 0; i < receiveData.length; i++) {
				if (receiveData[i].create_time) {
					receiveData[i].create_time = util.formatTimeTwo(receiveData[i].create_time, 'Y/M/D')
				}
				if (receiveData[i].taping) {
					var min = Math.floor(receiveData[i].record_time / 60) % 60
					var sec = receiveData[i].record_time % 60
					var send_time_min = ('0' + min + ':' + (sec < 10 ? '0' + sec : sec))
					receiveData[i].record_time = send_time_min
				}
			}
			if (page == 1) {
				that.data.receiveDataList = receiveData
			} else {
				if (e == 'page') { //重新加载分页获取
					that.data.receiveDataList = that.data.receiveDataList.concat(receiveData)
				}
			}
			that.setData({
				letter_img: app.data.letter_img,
				receiveDataList: that.data.receiveDataList,
				isShow: true,
				re_count: count,
			})
		})
	},
	// 授权按钮
	onGotUserInfo: function (e) {
		var state = e.detail.errMsg
		if (state == 'getUserInfo:ok') {
			app.wx_loginTip()
			var that = this
			app.wx_login().then(function (result) {
				app.wx_getUserInfo().then(function (result) {
					that.is_userInfo()
					app.wx_loginOk()
				})
			})
		} else {
			console.log("取消授权")
		}
	},
	selectKind() {
		this.setData({
			show_kind: !this.data.show_kind
		});
	},
	// 点击kind下拉列表 
	optionTap(e) {
		let index = e.currentTarget.dataset.index; //获取点击的下拉列表的下标 
		if (index == 0) { //	send
			this.data.show_send = true
			this.data.sendIndex = 0
			this.getSendData()
		} else {
			this.data.show_send = false
			this.data.receiveIndex = 0
			const userInfo = wx.getStorageSync('user_info')
			if (userInfo) {
				this.getReceiveData()
			} else {
				var receiveDataList = []
				receiveDataList = receiveDataList.concat(letter)
				this.setData({
					receiveDataList: receiveDataList
				})
			}
		}
		this.setData({
			kind_index: index,
			show_send: this.data.show_send,
			sendIndex: this.data.sendIndex,
			receiveIndex: this.data.receiveIndex,
			show_kind: false,
			play_time_min: '',
			page: 1,
		})
	},
	sendChange(e) {
		var sendIndex = e.detail.current
		var i = this.data.page * 20
		this.setData({
			sendIndex: sendIndex,
			show_kind: false
		})

		if (sendIndex < (i - 1)) { //19  39  59
			return
		}
		this.data.page = this.data.page + 1
		this.getSendData('page')

	},
	receiveChange(e) {
		var receiveIndex = e.detail.current
		var i = this.data.page * 20
		this.setData({
			receiveIndex: receiveIndex,
			show_kind: false
		})
		// 19、39、59
		if (receiveIndex < (i - 1)) {
			return
		}
		this.data.page = this.data.page + 1
		this.getReceiveData('page')
	},
	// 留言详情
	toDetail: function (e) {
		var kind = e.currentTarget.dataset.target // receive 、 send
		var id = e.currentTarget.dataset.id
		var idx = (this.data.show_send ? this.data.sendIndex : this.data.receiveIndex)
		var is_login = this.data.show_login
		var suiji = this.data.suiji
		wx.navigateTo({
			url: '/pages/detail/index?id=' + id + '&kind=' + kind + '&idx=' + idx + '&suiji=' + suiji,
		})
		this.setData({
			show_kind: false
		})
	},
	onShareAppMessage: function (e) {
		return {
			title: "同学录",
			path: "pages/check-friend/index?share_id=" + this.data.user_info.id + '&share_name=' + this.data.user_info.username,
			imageUrl: 'https://classmate.zhicloudcn.com/api//202005220931230268237.png',
			success: function (res) {
				wx.showToast({
					title: '分享成功',
					duration: 1500,
				})
			}
		};
	},
	onPullDownRefresh: function () {
		wx.showLoading({
			title: '重新加载中',
			mask: true
		})
		setTimeout(() => {
			this.setData({
				receiveIndex: 0,
				sendIndex: 0,
				page: 1,
			})
			this.getReceiveData('Refresh')
			this.getSendData('Refresh')
			wx.stopPullDownRefresh()
		}, 1500);

	},
	toHaibao() {
		var that = this
		// 'd1KQ36Nqy5uXsQahV1Ri3pMfm5W-5ehEf1qnq6_SmZk',
		wx.requestSubscribeMessage({
			tmplIds: ['d1KQ36Nqy5uXsQahV1Ri3qORwYl562FwO1cK0mE9hDQ',
				'f9ANDdl3vReN2pk2EL4Qw8F3WlIJwPTmBgDgg4pTcrQ'
			],
			success(res) {
				if (that.data.user_info) {
					wx.showLoading({
						title: '跳转中...',
						mask: true,
						duration: 1500,
					})
					setTimeout(() => {
						wx.navigateTo({
							url: '/pages/Haibao/haibao',
						})
					}, 1500);
				}
			},
			fail(err) {
				console.log(err, '订阅err')
			}
		})
	},
	onHide() {
		this.setData({
			show_kind: false,
		})
	},
})