const app = getApp()

Page({
   data: {
      member_id: '', //主用户id
      token: '',
      page:1,
   },
   onLoad(e){
      this.setData({
         more:true,
         id:e.id,
      })
      this.getUserInfo(e.id)
   },
   onShow: function () { 
     
   },
   getUserInfo(id) {
      var that = this
      var member_id =id
      var page =that.data.page
      if(page ==1){
         that.data.record_info = []
      }
      var url='Record/index?member_id='+  member_id + "&page="+page
      app.get_request(url).then(function(res){
         var record_info = res.data.data.list
         if(record_info.length < 20 && page>0){
            that.data.more = false
         }
         that.data.record_infoList=that.data.record_info.concat(record_info)
         // that.data.record_info=that.data.record_info.concat(record_info)
         that.setData({
            record_info: record_info,
            record_infoList:that.data.record_infoList,
            more:that.data.more,

         })
      })
   },
   onReachBottom(){
      if(!this.data.more){
			return
      }
      this.data.page = this.data.page + 1
		setTimeout(() => {
         this.getUserInfo(this.data.id)
    	}, 1000)		
   },
})