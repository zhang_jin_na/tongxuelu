var util = require('../../util.js');
const app = getApp()

Page({
	data: {
		page:1,
	},
	onLoad: function (e) {
		var member_id = e.member_id
		this.setData({
			more: true,
			member_id:member_id,
		})
		this.getredRecord(member_id)
	},
	onShow: function () {
		
	},
	getredRecord(member_id) {
		var that = this
		var page =that.data.page
      if(page ==1){
         that.data.redData = []
		}
		var url='Redrecord/index?member_id='+ member_id + "&page="+page
		app.get_request(url).then(function(res){
			var redData = res.data.data.list
			if(redData.length < 20 && page>0){
				that.data.more = false
			}
			for (var i = 0; i < redData.length; i++) {
				redData[i].create_time = util.formatTimeTwo(redData[i].create_time, 'Y.M.D h:m')
			}
			that.data.redDataList=that.data.redData.concat(redData)
			that.setData({
				redData: redData,
				redDataList:that.data.redDataList,
				more:that.data.more,
			})
			// console.log(page,that.data.redData)
		})
	},
	onReachBottom(){
      if(!this.data.more){
			return
      }
      this.data.page = this.data.page + 1
		setTimeout(() => {
         this.getredRecord(this.data.member_id)
    	}, 1000)		
   },
})